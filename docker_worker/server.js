'use strict'

const express = require('express');
const path = require('path');

//const amqp = require('amqplib');

const port = 3000;

const app = express();

app.use(express.json());
app.use('/css', express.static(path.join(__dirname, 'node_modules/bootstrap/dist/css')))
app.use('/js', express.static(path.join(__dirname, 'node_modules/bootstrap/dist/js')))
app.use('/js', express.static(path.join(__dirname, 'node_modules/jquery/dist')))

app.use(express.urlencoded({extended: true}));

//#region Routes
const platsRouter = require('./routes/plat.js');
const productController = require('./controllers/plat.controller.js');

//#endregion

// Crée une route pour la page d'accueil '/'
app.get('/', (req, res) => {
    //init database if necessary
    //productController.getAll;
    //res.sendFile('./views/index.html');
    res.sendFile(__dirname + '/views/index.html');
});

app.get('/api-doc', (req, res) => {
    //res.sendFile('./views/index.html');
    res.sendFile(__dirname + '/views/apidoc.html');
});

app.use('/api', platsRouter);

  app.listen(port, ()=>{
    console.log('server is up !');
});

//flags
// amqp.connect('amqp://localhost:5672')
//   .then(conn => {
//     return conn.createChannel();
//   })
//   .then(ch => {
//     const queue = 'myQueueTest';
//     const message = 'Hello, World!';
//     const options = { persistent: true };
//     return ch.sendToQueue(queue, Buffer.from(message), options);
//   })
//   .then(() => {
//     console.log('Message sent with persistent flag');
//   })
//   .catch(console.warn);

// amqp.connect('amqp://localhost:5672')
//   .then(conn => {
//     return conn.createChannel();
//   })
//   .then(ch => {
//     const queue = 'myQueueTest';
//     return ch.consume(queue, (msg) => {
//         console.log('Received persistent message:', msg.content.toString());
//     //   if(msg.properties.persistent) {
//     //     console.log('Received persistent message:', msg.content.toString());
//     //   }
//     });
//   })
//   .then(() => {
//     console.log('Consumer created');
//   })
//   .catch(console.warn);

module.exports = app;
