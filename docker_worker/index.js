//const express = require('express)const app = express()app.listen(8080, () => {  console.log("Serveur prêt")}')
var express = require("express");
var app = express();
var router = express.Router();

//const MONGODB_URI = "mongodb://localhost:27017/docker_worker"
//const MONGODB_URI = "localhost:81/docker_worker"

// connect to mongo db
// mongoose.connection.openUri(MONGODB_URI, { //process.env.MONGODB_URI
//     autoIndex: true,
//     keepAlive: true,
//     useNewUrlParser: true,
//     useUnifiedTopology: true,
//   });

// mongoose.connection.on('connected', () => console.info('Mongoose connected.'));

// mongoose.connection.on('error', (err) => {
//   throw new APIError(
//     `Mongoose connection error: ${err}`,
//     httpStatus.INTERNAL_SERVER_ERROR
//   );
// });

// mongoose.connection.on('disconnected', () =>
//   console.info('Mongoose disconnected.')
// );

//init connection redis db
// const redis = require('redis');
// const client = redis.createClient({
//     socket: {
//         host: 'localhost',
//         port: '8082'
//     },
//     password: ''
// });

// client.on('error', err => {
//     console.log('Error ' + err);
// });

// client.on('connect', function() {
//     console.log('Connected!');
//   });

var path = __dirname + './';

const PORT = 81;
const HOST = '0.0.0.0';

router.use(function (req,res,next) {
    console.log("/" + req.method);
    next();
  });

router.get("/api",function(req,res){
    res.sendFile(path + "index.html");
});

router.get("/api/plats",function(req,res){
    res.sendFile(path + "index.html");
});
  
app.use(express.static(path));
app.use("/api", router);

app.listen(81, function(){
    console.log('server is up !')
})

// https://www.cloudamqp.com/blog/how-to-run-rabbitmq-with-nodejs.html to connect with rabbitMQL

// listen on port config.port
// app.listen(process.env.PORT, () => {
//     console.info(`✅ Server started on port ${process.env.PORT} (${process.env.NODE_ENV}).`);
//   });
  
module.exports = app;