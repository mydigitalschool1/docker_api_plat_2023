// Include Sequelize module
const Sequelize = require('sequelize')
const Plat = require('./plat.model.js');

// Creating new Object of Sequelize
const sequelize = new Sequelize(
    'mds_api_plat', //db name
    'root', //username
    '', //password
     {
       host: 'localhost',
       dialect: 'mysql'
     }
   );

sequelize.authenticate().then(() => {
    console.log('Connection has been established successfully.');
 }).catch((error) => {
    console.error('Unable to connect to the database: ', error);
 });

//init db
sequelize.query('CREATE DATABASE IF NOT EXISTS mds_api_plat;').then(() => {
   Plat.findAll().then(plat => {
      if(plat.length <= 0){
         Plat.bulkCreate([
              { name: "Tartiflette", qte: 0, plat_status: "sold out" },
              { name: "Couscous", qte: 7, plat_status: "dispo"},
              { name: "Salade", qte: 2, plat_status: "dispo" },
          ]).then(() => console.log("Plats have been saved")); 
      }else{
         console.log('Connect to db');
      }
  });
})

sequelize.sync().then(console.log('DB is synced'));

// Exporting the sequelize object. 
// We can use it in another file
// for creating models
module.exports = sequelize