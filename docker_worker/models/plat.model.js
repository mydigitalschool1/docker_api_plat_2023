const { Sequelize, DataTypes } = require("sequelize");

const sequelize = new Sequelize(
    'mds_api_plat', //db name
    'root', //username
    '', //password
     {
       host: 'localhost',
       dialect: 'mysql'
     }
);

const Plat = sequelize.define("plats", {
    name: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: true
    },
    qte: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    plat_status: {
      type: DataTypes.STRING,
      allowNull: true
    }
 });

 sequelize.sync().then(() => {
    console.log('Plat table created successfully!');
 }).catch((error) => {
    console.error('Unable to create table : ', error);
 });

 module.exports = Plat