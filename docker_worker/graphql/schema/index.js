const { buildSchema } = require("graphql")

module.exports = buildSchema(`

  type Plat {
    _id: ID!
    name: String!
    body: String!
    createdAt: String!
  }


  input Ingredient {
    name: String!
    body: String!
  }

  type Query {
    plats:[Plat!]
  }

  type Mutation {
    createArticle(plat:Ingredient): Plat
  }

  schema {
    query: Query
    mutation: Mutation
  }
`)