const { Sequelize, DataTypes } = require("sequelize");
const sequelize = require("../models/database.js");
const Plat = require('../models/plat.model.js');
//const amqp = require('amqplib');

exports.getAll = (req, res, next) => {
    Plat.findAll().then(plat => {
        if(plat.length < 0){
            Plat.bulkCreate([
                { name: "Tartiflette", qte: 0 },
                { name: "Couscous", qte: 7},
                { name: "Salade", qte: 2 },
            ]).then(() => console.log("Plats have been saved")); 
        }else{
        res.json(plat);
        }
    }).catch(next);
};

exports.get = async (req, res, next) => {
    const id = req.params.platId;
    const plat = await Plat.findByPk(id)
        .catch(next);
    if(plat) {
        res.json(plat);
    } else {
        res.status(404).send(`User with id ${id} not found!`);
    }
};

exports.create = (req, res, next) => {
    const newPlat = req.body;
    let platList = [];
    //avoid name duplicates
    Plat.findAll().then(async (plat) => {
        plat.forEach(element => platList.push(element.name.toLowerCase()));
        if(!platList.includes(newPlat.name.toLowerCase())){
            Plat.create(req.body).then(p => res.json(p))
            .catch(next);
        }else{
            res.status(400).send("Un plat avec la même dénomination existe déjà.");
        }
    }).catch(next);    
};

exports.update = (req, res, next) => {
    const newPlat = req.body;
    const id = req.params.platId;
    //list of products
    Plat.findAll().then(async (plat) => {
        plat.forEach(element => platList.push(element.name));
        if(!platList.includes(newPlat.name)){
            Plat.findByPk(id).then(async (plat) => {
                if (plat) {
                    //prevent id change
                    newPlat.id = plat.id;
                    await Plat.update(newPlat, {where: {id}})
                    res.status(200).send("Plat modifié avec succès !");
                }else {
                    res.status(404).send();
                }
            }).catch(next);
        }else{
            res.status(400).send("Un plat avec la même dénomination existe déjà.");
        }
    }).catch(next);
};

exports.updateStock = (req, res, next) => {
    const id = req.params.productId;
    const newQte = req.body.qte;
    Product.findByPk(id).then(async (prd) => {
        if (prd) {
            //change only quantity
            await Product.update({ qte : newQte }, {where: {id}});
            res.status(200).send("Stock mis à jour avec succès !");
        }else {
            res.status(404).send();
        }
    }).catch(next);
}

exports.add = async (req, res, next) => {
    const id = 1;
    const qte = req.params.qte;
    const plat = await Plat.findByPk(id)
        .catch(next);
    if(plat) {
        //send a flag
        // amqp.connect('amqp://localhost:5672')
        // .then(conn => {
        //     return conn.createChannel();
        // })
        // .then(ch => {
        //     const queue = 'myQueueTest';
        //     const message = 'dispo!';
        //     const options = { persistent: true };
        //     return ch.sendToQueue(queue, Buffer.from(message), options);
        // })
        // .then(() => {
        //     console.log('Message sent with persistent flag');
        // })
        // .catch(console.warn);
        res.json(plat);
    } else {
        res.status(404).send(`Plat with id ${id} not found!`);
    }
};

exports.updateStockAdd = (req, res, next) => {
    const id = req.body.id;
    let newQte = req.body.qte;
    if( !isNaN(newQte) && newQte > 0){
        Product.findByPk(id).then(async (prd) => {
            if (prd) {
                //change only quantity
                newQte = prd.qte + newQte;
                await Product.update({ qte : newQte }, {where: {id}});
                res.status(200).send("Stock mis à jour avec succès !");
            }else {
                res.status(404).send("can't find plat.");
            }
        }).catch(next);
    }else{
        res.status(404).send("Stock must be a positive integer.");
    }
}

exports.updateStockMinus = (req, res, next) => {
    const id = req.body.id;
    let newQte = req.body.qte;
    if( !isNaN(newQte) && newQte > 0){
        Product.findByPk(id).then(async (prd) => {
            if (prd) {
                //change only quantity
                newQte = prd.qte - newQte;
                if(newQte < 0) newQte = 0;
                await Product.update({ qte : newQte }, {where: {id}});
                res.status(200).send("Stock mis à jour avec succès !");
            }else {
                res.status(404).send("can't find plat.");
            }
            res.status(404).send("id = "+id);
        }).catch(next);
    }else{
        res.status(404).send("Stock must be a positive integer.");
    }
}

exports.deleteProduct = (req, res, next) => {
    const id = req.params.productId;
    Product.destroy({where:{id}}).then(res.status(200).send("Plat supprimé avec succès !")).catch(next);
};
